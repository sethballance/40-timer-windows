﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;


namespace _40timer
{
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null || DateTime.Parse(value.ToString()) == DateTime.Parse("1/1/0001 12:00:00 AM"))
                return null;

            var dt = DateTime.Parse(value.ToString());
            return dt.ToString("MMM d, h:mm tt");
        }

        // No need to implement converting back on a one-way binding
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

}
