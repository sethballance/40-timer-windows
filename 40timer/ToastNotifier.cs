﻿using System;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace _40timer
{
    public class ToastNotifier
    {
        public static void SendToast(string title, string content = "", int timeToShowInSeconds = 10)
        {
            string toastVisual =
            $@"<visual>
              <binding template='ToastGeneric'>
                <text>{title}</text>
                <text>{content}</text>
              </binding>
            </visual>";
            string toastXmlString =
$@"<toast>
    {toastVisual}
</toast>";
            XmlDocument toastXml = new XmlDocument();
            toastXml.LoadXml(toastXmlString);

            // Generate toast
            var toast = new ToastNotification(toastXml) { ExpirationTime = DateTime.Now.AddSeconds(timeToShowInSeconds) };
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }
    }
}
